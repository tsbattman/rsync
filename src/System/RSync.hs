
module System.RSync (
    RSyncLocation(..)
  , RSyncOpts(..)
  , FlagLevel(..)
  , InfoFlag(..)
  , InfoLevel
  , rsync
  ) where

import Control.Exception (bracketOnError)
import Data.Monoid (First(..))
import System.IO (hClose, hPutStr)
import System.Exit (ExitCode)
import Text.Printf

import System.Process (CreateProcess(..), StdStream(..), createProcess_, proc, terminateProcess, waitForProcess)

data RSyncLocation = RSyncLocalPath FilePath
  | RSyncRemotePath (Maybe String) String FilePath
  | RSyncRshPath (Maybe String) String FilePath
  deriving (Eq)

instance Show RSyncLocation where
  show = rsyncLocation

rsyncLocation :: RSyncLocation -> String
rsyncLocation (RSyncLocalPath path) = path
rsyncLocation (RSyncRemotePath user host path) = case user of
  Just u -> printf "%s@%s::%s" u host path
  Nothing -> printf "%s::%s" host path
rsyncLocation (RSyncRshPath user host path) = case user of
  Just u -> printf "%s@%s:%s" u host path
  Nothing -> printf "%s:%s" host path

newtype FlagLevel = FlagLevel Int
  deriving (Eq, Ord, Show, Read)

data InfoFlag =
    InfoFlagAll
  | InfoFlagNone
  | InfoFlagBackup
  | InfoFlagCopy
  | InfoFlagDel
  | InfoFlagFList
  | InfoFlagMisc
  | InfoFlagMount
  | InfoFlagName
  | InfoFlagProgress
  | InfoFlagRemove
  | InfoFlagSkip
  | InfoFlagStats
  | InfoFlagSymSafe
  deriving (Eq, Show, Read)

type InfoLevel = (InfoFlag, FlagLevel)

infoLevelOpt :: InfoLevel -> String
infoLevelOpt (f, FlagLevel lvl) = printf "%s%d" nm lvl
  where
    nm = case f of
      InfoFlagAll -> "ALL"
      InfoFlagNone -> "NONE"
      InfoFlagBackup -> "BACKUP"
      InfoFlagCopy -> "COPY"
      InfoFlagDel -> "DEL"
      InfoFlagFList -> "FLIST"
      InfoFlagMisc -> "MISC"
      InfoFlagMount -> "MOUNT"
      InfoFlagName -> "NAME"
      InfoFlagProgress -> "PROGRESS"
      InfoFlagRemove -> "REMOVE"
      InfoFlagSkip -> "SKIP"
      InfoFlagStats -> "STATS"
      InfoFlagSymSafe -> "SYMSAFE"

infoLevelOpts :: [InfoLevel] -> String
infoLevelOpts = foldr (\f s -> infoLevelOpt f ++ s) ""

data RSyncOpts =
    RSyncArchive
  | RSyncRecursive
  | RSyncRelative
  | RSyncDelete
  | RSyncCompress
  | RSyncPasswordFile FilePath
  | RSyncPassword String
  | RSyncInclude String
  | RSyncExclude String
  | RSyncRsh String
  | RSyncInfo [InfoLevel]
  | RSyncVerbose

rsyncOption :: RSyncOpts -> String
rsyncOption opt = case opt of
  RSyncArchive -> "-a"
  RSyncRecursive -> "-r"
  RSyncRelative -> "-R"
  RSyncDelete -> "--delete"
  RSyncCompress -> "-z"
  RSyncPasswordFile path -> printf "--password-file=%s" path
  RSyncPassword _ -> "--password-file=-"
  RSyncInclude p -> printf "--include=%s" p
  RSyncExclude p -> printf "--exclude=%s" p
  RSyncRsh p -> printf "--rsh=%s" p
  RSyncInfo flags -> printf "--info=%s" (infoLevelOpts flags)
  RSyncVerbose -> "-v"

rsyncPassword :: [RSyncOpts] -> Maybe String
rsyncPassword = getFirst . foldMap (First . pwd)
  where
    pwd (RSyncPassword p) = Just p
    pwd _ = Nothing

rsync :: [RSyncOpts] -> RSyncLocation -> RSyncLocation -> IO ExitCode
rsync rsyncOpts src dest = bracketOnError (createProcess_ rsyncEXE (proc rsyncEXE opts) { std_in = CreatePipe }) cpTerm cpRun
  where
    cpRun (mstin, _, _, ph) = do
      case mstin of
        Just stin -> mapM_ (hPutStr stin) (rsyncPassword rsyncOpts) >> hClose stin
        Nothing -> return ()
      waitForProcess ph
    cpTerm (_, _, _, ph) = terminateProcess ph
    rsyncEXE = "rsync"
    opts = map rsyncOption rsyncOpts ++ [rsyncLocation src, rsyncLocation dest]
